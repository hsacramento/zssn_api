Rails.application.routes.draw do
  # API ROUTES
  namespace :api, defaults: { format: 'json' } do

    resources :items, only: :index

    resources :survivors do
      member do
        patch :update_location
      end

      collection do
        post  :notify_contamination
      end
    end

    namespace :reports do
      get :infected
      get :non_infected
      get :avarage_of_resources
      get :points_lost_because_infected_survivor
    end

    post :trade, to: 'traders#trade'
  end
end
