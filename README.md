# ZSSN (Zombie Survival Social Network)

_System to share resources between non-infected humans_

## Requirements

* [Ruby](https://www.ruby-lang.org) (2.5.1 last stable)
* [Rails](http://rubyonrails.org/)

## Running Project


```shell
bundle
bundle exec rails db:create
bundle exec rails db:migration
bundle exec rails db:seed
```

## REST Endpoints

`Items` ```GET /api/items.json```

_List all resources to get correct id_

`Survivors` ```POST /api/survivors.json```

_Example curl request to create a survivor with inventory_

```bash
curl -X POST \
  http://example.org/api/survivors.json \
  -H 'Content-Type: application/json' \
  -d '{
  "name": "Meg",
  "age": 31,
  "gender": "female",
  "last_location": [-17.5400307, -39.7421812],
  "inventory_attributes": {
        "inventory_resources_attributes": [
            {"item_id": 1 },
            {"item_id": 2 }
        ]
  }
}'
```

_Example curl request to update a survivor information_

`Survivors` ```PUT /api/survivors/${id}.json```

```bash
curl -X PATCH \
  http://example.org/api/survivors/${id}.json \
  -H 'Content-Type: application/json' \
  -d '{
  "name": "Meg",
  "age": 31,
  "gender": "female",
  "last_location": [1, 2]
}'
```

_Request to destroy a survivor_

`Survivors` ```DESTROY /api/survivors/${id}.json```

_Example curl request to update location_

```bash
curl -X PATCH \
  http://example.org/api/survivors/${id}/update_location.json \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 2468b8ac-9154-4f06-9e42-f45c2fa4a2b1' \
  -d '{
  "last_location": [1, 2]
}'
```

_Request to flag a survivor as infected_

`Survivors` ```POST /api/survivors/notify_contamination.json```

```bash
curl -X POST \
  'http://localhost:3000/api/survivors/notify_contamination.json?survivor_id=2&notifier_id=4' \
```

_Request to trade resources between to survivors_

`Survivors` ```POST /api/trade.json```

```bash
curl -X POST \
  http://example.org/api/trade.json \
  -H 'Content-Type: application/json' \
  -d '{
  "trade": {
    "buyer": {
      "id": 4,
      "resources": [
        { 
          "item_id": 4,
          "quantity": 1
        },
        { 
          "item_id": 2,
          "quantity": 1
        }
      ]
    },
    "trader": {
      "id": 3,
      "resources": [
        {
          "item_id": 1,
          "quantity": 6
        }
      ]
    }
  }
}'
```

`Reports` ```GET /api/reports/non_infected.json```

_List percentage of non infected survivors_ 

`Reports` ```GET /api/reports/infected.json```

_List percentage of infected survivors_ 

`Reports` ```GET /api/reports/avarage_of_resources.json```

_List percentage of average amount of each kind of resource by survivor_ 

`Reports` ```GET /api/reports/points_lost_because_infected_survivor.json```

_Points lost because of infected survivor_

