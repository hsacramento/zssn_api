# Items

if Item.all.empty?
  Item.create(
    [
      {name: 'ammunition', value: 1},
      {name: 'medication', value: 2},
      {name: 'food',       value: 3},
      {name: 'water',      value: 4}
    ]
  )
  puts "--> Created default Items"
end