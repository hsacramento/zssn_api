class CreateContaminations < ActiveRecord::Migration[5.1]
  def change
    create_table :contaminations do |t|
      t.references :survivor, foreign_key: true
      t.integer :notifier_id, null: false

      t.timestamps
    end
  end
end
