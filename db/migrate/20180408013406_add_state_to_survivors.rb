class AddStateToSurvivors < ActiveRecord::Migration[5.1]
  def change
    add_column :survivors, :state, :string
  end
end
