class ChangeLatitudeLongitudeToLastPositionInSurvivors < ActiveRecord::Migration[5.1]
  def change
    remove_column :survivors, :latitude
    remove_column :survivors, :longitude
    add_column    :survivors, :last_location, :decimal, precision: 10, scale: 6, array: true
  end
end
