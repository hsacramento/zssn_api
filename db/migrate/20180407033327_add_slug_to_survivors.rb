class AddSlugToSurvivors < ActiveRecord::Migration[5.1]
  def change
    add_column :survivors, :slug, :string
    add_index :survivors, :slug, unique: true
  end
end
