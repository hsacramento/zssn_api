class MoveQuantityFormInventoryToInventoryResources < ActiveRecord::Migration[5.1]
  def change
    remove_column :inventories, :quantity
    add_column :inventory_resources, :quantity, :integer, null: false, default: 0
  end
end
