class AddCounterCacheToSurvivors < ActiveRecord::Migration[5.1]
  def change
    add_column :survivors, :contaminations_count, :integer, default: 0
  end
end
