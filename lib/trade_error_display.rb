module TradeErrorDisplay
  @@errors = {
    Trade::Error::SurvivorNotFound => "Shit! Him didn't come? Maybe was died.",
    Trade::Error::UnpayedOffer => "Are you kidding me? You cannot pay the bill."
  }

  def self.message(error)
    @@errors[error.class]
  end
end