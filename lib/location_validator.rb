class LocationValidator < ActiveModel::Validator
  def validate(record)
    unless record.last_location && record.last_location.size == 2
      record.errors.add(:last_location, 'is invalid') 
    end
  end
end