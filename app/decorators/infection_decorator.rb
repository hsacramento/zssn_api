class InfectionDecorator < SimpleDelegator  
  MESSAGES = ["Ok! Was he bitten?", "Truth? Let's cut the arm!", "Shit! Without salvation!"]

  def display_message    
    self.contaminated? ? MESSAGES.sample : 'Hunger! Brain! Eat! Wan...tt!'
  end
end 