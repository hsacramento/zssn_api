module Stateable
  extend ActiveSupport::Concern

  included do
    include AASM

    aasm column: :state do
      state :clean, initial: true
      state :contaminated, :infected

      event :contamine do
        transitions from: :clean, to: :contaminated
      end

      event :infect do
        transitions from: :contaminated, to: :infected
      end
    end
  end
end