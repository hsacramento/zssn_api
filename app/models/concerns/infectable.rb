module Infectable
  extend ActiveSupport::Concern

  MAX_CONTAMINATION_WARNINGS = 3

  included do
    has_many :contaminations
    has_many :notifiers
  end

  def notify_infection!(survivor)
    return unless survivor
    
    unless survivor.contaminations_exceeded?
      Contamination.create(notifier_id: self.id, survivor_id: survivor.id)
      survivor.contamine! if survivor.clean?
    end

    survivor.infect! if survivor.contaminations_exceeded? && survivor.contaminated?
  end

  def contaminations_exceeded?
    self.reload
    self.contaminations_count >= MAX_CONTAMINATION_WARNINGS
  end
end
