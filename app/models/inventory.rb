# == Schema Information
#
# Table name: inventories
#
#  id          :integer          not null, primary key
#  survivor_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Inventory < ApplicationRecord
  # relationships
  belongs_to :survivor
  has_many :inventory_resources
  has_many :items, through: :inventory_resources

  accepts_nested_attributes_for :inventory_resources

  # validations
  validates :survivor, presence: true
end
