# == Schema Information
#
# Table name: contaminations
#
#  id          :integer          not null, primary key
#  survivor_id :integer
#  notifier_id :integer          not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Contamination < ApplicationRecord
  # relationships
  belongs_to :survivor, counter_cache: true
  belongs_to :notifier, class_name: 'Survivor', foreign_key: 'notifier_id'

  # validations
  validate :unique_notification_per_survivor

  private

  def unique_notification_per_survivor
    self.errors.add(:survivor, 'you have already reported the infection to this survivor') if notified_survivor?
  end

  def notified_survivor?
    self.class.where(survivor_id: survivor_id, notifier_id: notifier_id).present?
  end
end
