# == Schema Information
#
# Table name: survivors
#
#  id                   :integer          not null, primary key
#  name                 :string           not null
#  age                  :integer          not null
#  gender               :string           not null
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  slug                 :string
#  last_location        :decimal(10, 6)   is an Array
#  contaminations_count :integer          default(0)
#  state                :string
#

class Survivor < ApplicationRecord
  include Stateable
  include Infectable

  extend Enumerize
  extend FriendlyId
  
  enumerize :gender, in: [:male, :female]
  friendly_id :name, use: :slugged

  # relationships
  has_one :inventory, dependent: :nullify
  has_many :inventory_resources, through: :inventory
  has_many :contamination, dependent: :nullify
  has_many :notifiers, -> { distinct }, through: :contaminations

  accepts_nested_attributes_for :inventory

  # scope
  scope :non_infected, -> { where.not(state: :infected) }
  scope :by_resource, -> (resource_name) { joins(inventory: :items).where(items: { name: resource_name }) }

  # validations
  validates :name, :gender, :age, :last_location, presence: true
  validates :age, inclusion: { in: 1..100 }
  validates_with LocationValidator

  # instance methods
  def as_json(*)
    super.except("created_at", "updated_at").tap do |hash|
      hash["resources"] = resources
    end
  end

  # class methods
  def self.percental_of_infected_survivors
    self.format_percentage( (self.infected.count.to_f / self.all.count.to_f) * 100 )
  end

  def self.percental_of_non_infected_survivors
    self.format_percentage( (self.non_infected.count.to_f / self.all.count.to_f) * 100 )
  end

  def self.format_percentage(percentage)
    "#{percentage}%"
  end

  def self.percentage_of_resource(resource_name)
    self.format_percentage( (self.by_resource(resource_name).count.to_f / self.all.count.to_f) * 100 )
  end

  def self.value_of_lost_resources
    self.infected.joins(inventory: :items).pluck(:value).inject(:+)
  end

  private

  def resources
    self.inventory_resources
  end
end
