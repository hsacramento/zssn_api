# == Schema Information
#
# Table name: inventory_resources
#
#  id           :integer          not null, primary key
#  item_id      :integer
#  inventory_id :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  quantity     :integer          default(0), not null
#

class InventoryResource < ApplicationRecord
  # relationships
  belongs_to :item
  belongs_to :inventory

  # validations
  validates :item, :inventory, :quantity, presence: true

  # instance methods
  def as_json(*)
    super.except('created_at', 'updated_at', 'id', 'item_id', 'inventory_id').tap do |hash|
      hash['name'] = item.name
      hash['value'] = item.value
    end
  end
end
