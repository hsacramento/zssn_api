# == Schema Information
#
# Table name: items
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  value      :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Item < ApplicationRecord
  extend Enumerize
  enumerize :name, in: [:water, :food, :medication, :ammunition]

  # relationships
  has_many :inventory_resources
  has_many :inventories, through: :inventory_resources
  
  # validations
  validates :name, :value, presence: true
  validates :value, inclusion: { in: 1..4 }

  # instance methods
  def as_json(*)
    super.except("created_at", "updated_at")
  end
end
