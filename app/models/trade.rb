class Trade
  module Error
    class Standard < StandardError; end
    class UnpayedOffer < Standard; end
    class SurvivorNotFound < Standard; end
  end

  def initialize(buyer, trader)
    @buyer = Survivor.find_by(id: buyer[:id])
    @trader = Survivor.find_by(id: trader[:id])

    @buyer_offer = buyer[:resources]
    @trader_offer = trader[:resources]
  end

  def process
    raise Error::SurvivorNotFound unless @buyer.present? || @trader.present?
    complete_trade unless has_threat? || unfair_trade? || !valid_trade?
  end

  def valid_trade?
    offer_valid?(@buyer, @buyer_offer) && offer_valid?(@trader, @trader_offer)
  end

  def unfair_trade?
    offer_value(@buyer_offer) != offer_value(@trader_offer)
  end

  def has_threat?
    @buyer.infected? || @trader.infected?
  end

  private

  def complete_trade
    if pay_offer(@trader, @trader_offer) && pay_offer(@buyer, @buyer_offer)
      receive_offer(@buyer, @trader_offer) && receive_offer(@trader, @buyer_offer)
      true
    else
      raise Error::UnpayedOffer
      false
    end
  end

  def pay_offer(survivor, resources)
    paid = 0
    resources.each do |offer_resource|
        resource = survivor.inventory_resources.find_by(item_id: offer_resource[:item_id])
        resource.quantity -= offer_resource[:quantity].to_i
        if resource.save 
            paid += 1
        end
    end
    paid == resources.length
  end

  def receive_offer(survivor, resources)
    resources.each do |resource|
      inventory_resource = survivor.inventory_resources.find_or_initialize_by(item_id: resource[:item_id])
      inventory_resource.quantity = (inventory_resource.quantity || 0) + resource[:quantity].to_i
      inventory_resource.inventory_id = survivor.inventory.id
      inventory_resource.save
    end
  end

  def offer_valid?(survivor, offer)
    founded = 0

    offer.each do |o|
      survivor.inventory_resources.where(item_id: o[:item_id]).each do |resource|
        founded += 1 if resource.quantity >= o[:quantity].to_i  
      end
    end 

    founded == offer.length
  end

  def offer_value(resources)
    total_value = 0
    
    if resources.present?
        resources.each do |item|
          
          resource = Item.find_by(id: item[:item_id])
          if resource.present?
              total_value += resource.value * item[:quantity].to_i
          end
        end
    end

    total_value
  end
end
