module Api
  class TradersController < ApplicationController
    def trade
      buyer_params = trade_params[:buyer]
      trader_params = trade_params[:trader]
      trade = Trade.new(buyer_params, trader_params)

      if trade.process
        render json: { message: 'Best deal ever!' }, status: :ok
      else
        render json: { message: 'Cannot complete the trade!' }, status: :unprocessable_entity
      end

    rescue Trade::Error::Standard => exception
      render json: { error: TradeErrorDisplay.message(exception) }, status: :unprocessable_entity
    end

    def trade_params
      params.require(:trade).permit(
        buyer: [ :id, resources: [:item_id, :quantity] ], 
        trader: [ :id, resources: [:item_id, :quantity] ]
      )
    end
  end
end