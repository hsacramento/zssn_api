module Api
  class ReportsController < ApplicationController
    def infected
      render json: { percentage: Survivor.percental_of_infected_survivors }
    end

    def non_infected
      render json: { percentage: Survivor.percental_of_non_infected_survivors }
    end

    def avarage_of_resources
      render json: {
        water: Survivor.percentage_of_resource('water'),
        food: Survivor.percentage_of_resource('food'),
        medication: Survivor.percentage_of_resource('medication'),
        ammunition: Survivor.percentage_of_resource('ammunition')
      }
    end

    def points_lost_because_infected_survivor
      render json: { points: Survivor.value_of_lost_resources || 0 }
    end
  end
end