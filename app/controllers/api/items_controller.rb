module Api
  class ItemsController < ApplicationController
    def index
      @items = Item.all.page(params[:page])
      render json: @items, status: :ok
    end
  end
end