module Api
  class SurvivorsController < ApplicationController
    before_action :load_model, only: [:show, :update, :update_location, :destroy]

    def index
      @survivors = Survivor.all.page(params[:page])
      render json: @survivors, status: :ok
    end

    def show
      render json: @survivor, status: :ok
    end

    def create
      @survivor = Survivor.new(survivor_params)
      if @survivor.save
        render json: @survivor, status: :created
      else
        render json: ErrorSerializer.serialize(@survivor.errors), status: :unprocessable_entity
      end
    end

    def update
      params.delete(:inventory_attributes)
      if @survivor.update_attributes(survivor_params)
        render :show, status: :ok
      else
        render json: ErrorSerializer.serialize(@survivor.errors), status: :unprocessable_entity
      end
    end

    def destroy
      @survivor.destroy
      head :no_content
    end

    def update_location
      @survivor.last_location = params[:last_location]
      if @survivor.save
        render :show, status: :ok
      else
        render json: ErrorSerializer.serialize(@survivor.errors), status: :unprocessable_entity
      end
    end

    def notify_contamination
      @survivor = Survivor.friendly.find(params[:survivor_id])
      @notifier = Survivor.friendly.find(params[:notifier_id])

      if @notifier && @survivor
        @notifier.notify_infection!(@survivor)
        render json: { message: InfectionDecorator.new(@survivor).display_message }, status: :ok
      else
        render json: { message: "Survivor or Notifier not exist" }, status: :unprocessable_entity
      end  
    end

    private

    def survivor_params
      params.permit(
        :id, :name, :age, :gender, last_location: [],
        inventory_attributes: [ inventory_resources_attributes: [ :item_id, :quantity ] ]
      )
    end

    def load_model
      @survivor = Survivor.friendly.find(params[:id])
    end
  end
end
