# == Schema Information
#
# Table name: inventory_resources
#
#  id           :integer          not null, primary key
#  item_id      :integer
#  inventory_id :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  quantity     :integer          default(0), not null
#

FactoryBot.define do
  factory :inventory_resource do
    inventory
    item
    quantity { 1 }

    trait :resource_water do
      after :create do |inventory_resource|
        create(:water, inventory_resources: [inventory_resource])
      end
    end

    trait :resource_medication do
      after :create do |inventory_resource|
        create(:medication, inventory_resources: [inventory_resource])
      end
    end

    factory :inventory_resource_water, traits: [:resource_water]
    factory :inventory_resource_medication, traits: [:resource_medication]
  end
end
