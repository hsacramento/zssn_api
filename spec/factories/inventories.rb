# == Schema Information
#
# Table name: inventories
#
#  id          :integer          not null, primary key
#  survivor_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryBot.define do
  factory :inventory do
    survivor { FactoryBot.create(:survivor) }

    trait :inventory_with_resources do
      after(:create) do |inventory|
        create(:inventory_resource_water, inventory: inventory)
      end
    end

    trait :inventory_with_resources_medication do
      after(:create) do |inventory|
        create(:inventory_resource_medication, inventory: inventory)
      end
    end

    factory :inventory_resources, traits: [:inventory_with_resources]
    factory :inventory_resources_medication, traits: [:inventory_with_resources_medication]
  end
end
