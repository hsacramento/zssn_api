# == Schema Information
#
# Table name: survivors
#
#  id                   :integer          not null, primary key
#  name                 :string           not null
#  age                  :integer          not null
#  gender               :string           not null
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  slug                 :string
#  last_location        :decimal(10, 6)   is an Array
#  contaminations_count :integer          default(0)
#  state                :string
#

FactoryBot.define do
  factory :survivor do
    name            { Faker::Name.name }
    age             { Faker::Number.between(1, 100) }
    gender          { [:male, :female].sample }
    last_location   { [Faker::Address.latitude, Faker::Address.longitude] }

    trait :with_resources do
      after :create do |survivor|
        create :inventory_resources, survivor_id: survivor.id
      end
    end

    trait :with_resources_water do
      after :create do |survivor|
        create :inventory_resources_water, survivor_id: survivor.id
      end
    end

    trait :with_resources_medication do
      after :create do |survivor|
        create :inventory_resources_medication, survivor_id: survivor.id
      end
    end

    trait :infected do
      after :create do |survivor|
        survivor.state = :infected
        survivor.save(validate: false)
      end
    end

    factory :survivor_with_resources, traits: [:with_resources]
    factory :survivor_with_resources_medication, traits: [:with_resources_medication]
    factory :survivor_infected, traits: [:infected]
  end
end
