# == Schema Information
#
# Table name: items
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  value      :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryBot.define do
  factory :item do
    name       { [:water, :food, :medication, :ammunition].sample }
    value      { Faker::Number.between(1, 4) }

    trait :item_ammunition do
      value    { 1 }
      name     { :ammunition }
    end

    trait :item_medication do
      value    { 2 }
      name     { :medication }
    end

    trait :item_food do
      value    { 3 }
      name     { :food }
    end

    trait :item_water do
      value    { 4 }
      name     { :water }
    end

    factory :ammunition, traits: [:item_ammunition]
    factory :medication, traits: [:item_medication]
    factory :food,       traits: [:item_food]
    factory :water,      traits: [:item_water]
  end
end
