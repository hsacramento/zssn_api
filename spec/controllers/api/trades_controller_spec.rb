require "spec_helper"

RSpec.describe Api::TradersController, type: :controller do
  describe 'POST #trade' do
    context 'valid trade' do
      let!(:buyer) { FactoryBot.create(:survivor_with_resources) }
      let!(:trader) { FactoryBot.create(:survivor_with_resources) }

      let(:trade_params) { 
        {
          "trade": {
            "buyer": {
              "id": buyer.id,
              "resources": [
                { 
                  "item_id": buyer.inventory_resources.first.item_id,
                  "quantity": buyer.inventory_resources.first.quantity
                }
              ]
            },
            "trader": {
              "id": trader.id,
              "resources": [
                { 
                  "item_id": trader.inventory_resources.first.item_id,
                  "quantity": trader.inventory_resources.first.quantity
                }
              ]
            }
          }
        }
      }

      it 'trade survivors items' do
        post :trade, params: trade_params, format: :json

        expect(api_response[:message]).to eql('Best deal ever!')
      end
    end

    context 'invalid trade' do
      let!(:buyer) { FactoryBot.create(:survivor_with_resources) }
      let!(:trader) { FactoryBot.create(:survivor_with_resources_medication) }

      let(:trade_params) { 
        {
          "trade": {
            "buyer": {
              "id": buyer.id,
              "resources": [
                { 
                  "item_id": buyer.inventory_resources.first.item_id,
                  "quantity": buyer.inventory_resources.first.quantity
                }
              ]
            },
            "trader": {
              "id": trader.id,
              "resources": [
                { 
                  "item_id": trader.inventory_resources.first.item_id,
                  "quantity": trader.inventory_resources.first.quantity
                }
              ]
            }
          }
        }
      }

      it 'trade survivors items' do
        post :trade, params: trade_params, format: :json

        expect(api_response[:message]).to eql('Cannot complete the trade!')
      end
    end
  end
end