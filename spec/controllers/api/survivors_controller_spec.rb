require "spec_helper"

RSpec.describe Api::SurvivorsController, type: :controller do
  describe 'GET #index' do
    let!(:survivors) { FactoryBot.create_list(:survivor, 2) }

    it 'returns http success' do
      get :index, format: :json
      expect(response).to have_http_status(:success)
    end

    it 'assigns the index survivor to @survivor' do
      get :index, format: :json
      expect(assigns(:survivors)).to match_array(survivors)
    end
  end

  describe 'POST #create' do
    context 'with valid attributes' do
      let(:request) { post :create, params: attributes, format: :json }
      let(:attributes) { FactoryBot.attributes_for(:survivor) }

      it 'create a new survivor and return success http status' do
        expect{
          request
        }.to change(Survivor, :count).by(1)

        expect(response.status).to eq 201
      end

      context 'create resources' do
        before do
          attributes.merge!({
            inventory_attributes: {
              inventory_resources_attributes: [{
                item_id: FactoryBot.create(:food).id
              }]
            }
          })
        end

        it 'create a inventory for survivor' do
          request
          expected = Survivor.first
          expect(expected.inventory).to match Inventory.first
        end

        it 'create a resource for survivor' do
          request 
          expected = Survivor.first
          expect(expected.inventory_resources).to match InventoryResource.all
        end
      end
    end

    context 'with invalid attributes' do
      it "responds with 422" do
        params = FactoryBot.attributes_for(:survivor)
        params[:name] = nil
        params[:last_location] = []
        post :create, params: params, format: :json
        expect(response.status).to eq(422)
      end
    end
  end

  describe 'PATCH #update' do
    let!(:survivor) { FactoryBot.create(:survivor_with_resources) }
    let(:valid_attributes) { FactoryBot.attributes_for(:survivor) }

    context 'with valid attributes' do
      it 'updates only survivor attrs' do
        valid_attributes.merge!({
          inventory_attributes: {
            inventory_resources_attributes: [{
              item_id: FactoryBot.create(:water).id
            }]
          }
        })

        patch :update, params: { id: survivor.id }.merge(valid_attributes), format: :json
        expect(response).to be_success

        survivor.reload

        expect(survivor.inventory_resources.size).to eql(1)
      end
    end

    context 'with invalid attributes' do
      it 'return http status 422' do
        patch :update, params: { id: survivor.id, name: nil}, format: :json

        expect(response.status).to eq(422)
      end
    end
  end

  describe 'PATCH #update_location' do
    let!(:survivor) { FactoryBot.create(:survivor_with_resources) }

    context 'with valid attributes' do
      it 'update only last_location' do
        patch :update_location, params: { id: survivor.id, last_location: [1, 2], name: Faker::Name.name }, format: :json

        expect(response).to be_success
        survivor.reload

        expect(survivor.last_location).to eql([1, 2])
        expect(survivor.name).to eql(survivor.name)
      end
    end

    context 'with invalid attributes' do
      it 'return http status 422' do
        patch :update_location, params: { id: survivor.id, last_location: [] }, format: :json

        expect(response.status).to eq(422)
      end
    end
  end

  describe '#POST notify_contamination' do
    let(:survivor) { FactoryBot.create(:survivor) }
    let(:notifier) { FactoryBot.create(:survivor) }
    let(:expected_messages) { ["Ok! Was he bitten?", "Truth? Let's cut the arm!", "Shit! Without salvation!"] }

    context 'with valid params' do
      it 'creates a contamination' do
        expect {
          post :notify_contamination, params: { survivor_id: survivor.id, notifier_id: notifier.id }, format: :json
        }.to change(Contamination, :count).by(1)

        expect(expected_messages).to include(api_response[:message])
      end
    end

    context 'with last notification' do
      let(:notifier) { FactoryBot.create(:survivor) }
      let(:other_notifier) { FactoryBot.create(:survivor) }
      let(:another_notifier) { FactoryBot.create(:survivor) }

      it 'made survivor as a zoombie' do
        post :notify_contamination, params: { survivor_id: survivor.id, notifier_id: notifier.id }, format: :json
        post :notify_contamination, params: { survivor_id: survivor.id, notifier_id: other_notifier.id }, format: :json
        
        expect {
          post :notify_contamination, params: { survivor_id: survivor.id, notifier_id: another_notifier.id }, format: :json
        }.to change(Contamination, :count).by(1)

        expect(api_response[:message]).to eql('Hunger! Brain! Eat! Wan...tt!')
      end
    end
  end
end