# == Schema Information
#
# Table name: inventories
#
#  id          :integer          not null, primary key
#  survivor_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'spec_helper'

RSpec.describe Inventory, type: :model do
  describe 'relationships' do
    it { should belong_to(:survivor) }
    it { is_expected.to have_many :inventory_resources }
    it { is_expected.to have_many(:items).through(:inventory_resources) }
    it { should accept_nested_attributes_for :inventory_resources }
  end

  describe '#survivor' do
    it { should validate_presence_of(:survivor) }
  end

  describe 'validations' do
    let(:attributes) { FactoryBot.attributes_for(:inventory) }
    let(:invalid_attributes) { FactoryBot.attributes_for(:inventory, survivor: nil) }

    it 'has a valid factory' do
      expect(described_class.new(attributes)).to be_valid
    end

    it 'invalid without required attributes' do
      expect(described_class.new(invalid_attributes)).to_not be_valid
    end
  end
end
