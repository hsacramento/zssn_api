# == Schema Information
#
# Table name: items
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  value      :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'spec_helper'

RSpec.describe Item, type: :model do
  describe 'relationships' do
    it{ is_expected.to have_many :inventory_resources }
    it{ is_expected.to have_many(:inventories).through(:inventory_resources) }
  end

  describe '#name' do
    it { should validate_presence_of(:name) }
  end

  describe '#value' do
    it { should validate_inclusion_of(:value).in_range(1..4) }
    it { should validate_presence_of(:value) }
  end

  describe 'validations' do
    let(:attributes) { FactoryBot.attributes_for(:item) }
    let(:invalid_attributes) { FactoryBot.attributes_for(:item, name: nil) }

    it 'has a valid factory' do
      expect(described_class.new(attributes)).to be_valid
    end

    it 'invalid without required attributes' do
      expect(described_class.new(invalid_attributes)).to_not be_valid
    end
  end
end
