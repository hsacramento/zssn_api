# == Schema Information
#
# Table name: inventory_resources
#
#  id           :integer          not null, primary key
#  item_id      :integer
#  inventory_id :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  quantity     :integer          default(0), not null
#

require 'spec_helper'

RSpec.describe InventoryResource, type: :model do
  describe 'relationships' do
    it { should belong_to(:inventory) }
    it { should belong_to(:item) }
  end
  
  describe '#quantity' do
    it { should validate_presence_of(:quantity) }
  end
end
