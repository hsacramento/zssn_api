require 'spec_helper'

RSpec.describe Trade, type: :model do
  describe '.valid_trade?' do
    context "when is valid" do
      let!(:buyer) { FactoryBot.create(:survivor_with_resources) }
      let!(:trader) { FactoryBot.create(:survivor_with_resources_medication) }
      let(:buyer_params) { 
        {
          "id": buyer.id,
          "resources": [
            { 
              "item_id": buyer.inventory_resources.first.item_id,
              "quantity": buyer.inventory_resources.first.quantity
            }
          ]
        }
      }

      let(:trader_params) { 
        {
          "id": trader.id,
          "resources": [
            { 
              "item_id": trader.inventory_resources.first.item_id,
              "quantity": trader.inventory_resources.first.quantity
            }
          ]
        }
      }

      it 'return true' do
        trade = described_class.new(buyer_params, trader_params)
        
        expect(trade.valid_trade?).to be_truthy
      end
    end

    context "when is invalid" do
      let!(:buyer) { FactoryBot.create(:survivor_with_resources) }
      let!(:trader) { FactoryBot.create(:survivor_with_resources_medication) }
      let(:buyer_params) { 
        {
          "id": buyer.id,
          "resources": [
            { 
              "item_id": buyer.inventory_resources.first.item_id,
              "quantity": 2
            }
          ]
        }
      }

      let(:trader_params) { 
        {
          "id": trader.id,
          "resources": [
            { 
              "item_id": trader.inventory_resources.first.item_id,
              "quantity": 2
            }
          ]
        }
      }

      it 'return true' do
        trade = described_class.new(buyer_params, trader_params)
        
        expect(trade.valid_trade?).to be_falsey
      end
    end
  end

  describe '.unfair_trade?' do
    context "when is valid" do
      let!(:buyer) { FactoryBot.create(:survivor_with_resources) }
      let!(:trader) { FactoryBot.create(:survivor_with_resources_medication) }
      let(:buyer_params) { 
        {
          "id": buyer.id,
          "resources": [
            { 
              "item_id": buyer.inventory_resources.first.item_id,
              "quantity": buyer.inventory_resources.first.quantity
            }
          ]
        }
      }

      let(:trader_params) { 
        {
          "id": trader.id,
          "resources": [
            { 
              "item_id": trader.inventory_resources.first.item_id,
              "quantity": trader.inventory_resources.first.quantity
            }
          ]
        }
      }

      it 'return true' do
        trade = described_class.new(buyer_params, trader_params)
        
        expect(trade.unfair_trade?).to be_truthy
      end
    end

    context "when is invalid" do
      let!(:buyer) { FactoryBot.create(:survivor_with_resources) }
      let!(:trader) { FactoryBot.create(:survivor_with_resources) }
      let(:buyer_params) { 
        {
          "id": buyer.id,
          "resources": [
            { 
              "item_id": buyer.inventory_resources.first.item_id,
              "quantity": 2
            }
          ]
        }
      }

      let(:trader_params) { 
        {
          "id": trader.id,
          "resources": [
            { 
              "item_id": trader.inventory_resources.first.item_id,
              "quantity": 2
            }
          ]
        }
      }

      it 'return true' do
        trade = described_class.new(buyer_params, trader_params)
        
        expect(trade.unfair_trade?).to be_falsey
      end
    end
  end

  describe '.has_threat?' do
    context 'when one of survivors has infected' do
      let!(:buyer) { FactoryBot.create(:survivor_with_resources, :infected) }
      let!(:trader) { FactoryBot.create(:survivor_with_resources_medication, :infected) }
      let(:buyer_params) { 
        {
          "id": buyer.id,
          "resources": [
            { 
              "item_id": buyer.inventory_resources.first.item_id,
              "quantity": buyer.inventory_resources.first.quantity
            }
          ]
        }
      }

      let(:trader_params) { 
        {
          "id": trader.id,
          "resources": [
            { 
              "item_id": trader.inventory_resources.first.item_id,
              "quantity": trader.inventory_resources.first.quantity
            }
          ]
        }
      }

      it 'return true' do
        trade = described_class.new(buyer_params, trader_params)
        
        expect(trade.has_threat?).to be_truthy
      end
    end

    context 'when no one of survivors has infected' do
      let!(:buyer) { FactoryBot.create(:survivor_with_resources) }
      let!(:trader) { FactoryBot.create(:survivor_with_resources_medication) }
      let(:buyer_params) { 
        {
          "id": buyer.id,
          "resources": [
            { 
              "item_id": buyer.inventory_resources.first.item_id,
              "quantity": buyer.inventory_resources.first.quantity
            }
          ]
        }
      }

      let(:trader_params) { 
        {
          "id": trader.id,
          "resources": [
            { 
              "item_id": trader.inventory_resources.first.item_id,
              "quantity": trader.inventory_resources.first.quantity
            }
          ]
        }
      }

      it 'return false' do
        trade = described_class.new(buyer_params, trader_params)
        
        expect(trade.has_threat?).to be_falsey
      end
    end
  end

  describe '.process?' do
    context "when is valid" do
      let!(:buyer) { FactoryBot.create(:survivor_with_resources) }
      let!(:trader) { FactoryBot.create(:survivor_with_resources) }
      let(:buyer_params) { 
        {
          "id": buyer.id,
          "resources": [
            { 
              "item_id": buyer.inventory_resources.first.item_id,
              "quantity": buyer.inventory_resources.first.quantity
            }
          ]
        }
      }

      let(:trader_params) { 
        {
          "id": trader.id,
          "resources": [
            { 
              "item_id": trader.inventory_resources.first.item_id,
              "quantity": trader.inventory_resources.first.quantity
            }
          ]
        }
      }

      it 'return true' do
        trade = described_class.new(buyer_params, trader_params)
        
        expect(trade.process).to be_truthy
      end
    end

    context "when is invalid" do
      let!(:buyer) { FactoryBot.create(:survivor_with_resources) }
      let!(:trader) { FactoryBot.create(:survivor_with_resources_medication) }
      let(:buyer_params) { 
        {
          "id": buyer.id,
          "resources": [
            { 
              "item_id": buyer.inventory_resources.first.item_id,
              "quantity": 2
            }
          ]
        }
      }

      let(:trader_params) { 
        {
          "id": trader.id,
          "resources": [
            { 
              "item_id": trader.inventory_resources.first.item_id,
              "quantity": 2
            }
          ]
        }
      }

      it 'return true' do
        trade = described_class.new(buyer_params, trader_params)
        
        expect(trade.process).to be_falsey
      end
    end
  end
end