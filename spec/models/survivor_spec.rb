# == Schema Information
#
# Table name: survivors
#
#  id                   :integer          not null, primary key
#  name                 :string           not null
#  age                  :integer          not null
#  gender               :string           not null
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  slug                 :string
#  last_location        :decimal(10, 6)   is an Array
#  contaminations_count :integer          default(0)
#  state                :string
#

require 'spec_helper'

RSpec.describe Survivor, type: :model do
  describe 'relationships' do
    it { should have_one(:inventory) }
    it { should accept_nested_attributes_for :inventory }
    it { should have_many(:inventory_resources) }
  end

  describe '#name' do
    it { should validate_presence_of(:name) }
  end

  describe '#gender' do
    it { should enumerize(:gender).in(:male, :female) }
    it { should validate_presence_of(:gender) }
  end

  describe '#age' do
    it { should validate_inclusion_of(:age).in_range(1..100) }
    it { should validate_presence_of(:age) }
  end

  describe '#last_location' do
    it { should validate_presence_of(:last_location) }
  end

  describe 'validations' do
    let(:attributes) { FactoryBot.attributes_for(:survivor) }
    let(:invalid_attributes) { FactoryBot.attributes_for(:survivor, name: nil) }

    it 'has a valid factory' do
      expect(described_class.new(attributes)).to be_valid
    end

    it 'invalid without required attributes' do
      expect(described_class.new(invalid_attributes)).to_not be_valid
    end
  end

  describe 'infactable' do
    context 'first notification' do
      let(:survivor) { FactoryBot.create(:survivor) }
      let(:notifier) { FactoryBot.create(:survivor) }
      let(:other_notifier) { FactoryBot.create(:survivor) }

      it 'notify survivor contamination' do
        expect {
          notifier.notify_infection!(survivor)
        }.to change(Contamination, :count).by(1)

        survivor.reload

        expect(survivor.contaminations_count).to eql(1)
        expect(survivor.contaminated?).to be_truthy
      end

      it 'do not duplicate contamination notification' do
        notifier.notify_infection!(survivor)

        expect{
          notifier.notify_infection!(survivor)
        }.to_not change(Contamination, :count)

        survivor.reload

        expect(survivor.contaminations_count).to eql(1)
        expect(survivor.contaminated?).to be_truthy
      end
    end

    context 'second contamination notification' do
      let(:survivor) { FactoryBot.create(:survivor) }
      let(:notifier) { FactoryBot.create(:survivor) }
      let(:other_notifier) { FactoryBot.create(:survivor) }

      it 'create a new contamination notification for survivor' do
        notifier.notify_infection!(survivor)
        notifier.notify_infection!(survivor)

        expect{
          other_notifier.notify_infection!(survivor)
        }.to change(Contamination, :count)

        survivor.reload

        expect(survivor.contaminations_count).to eql(2)
        expect(survivor.contaminated?).to be_truthy
      end
    end

    context 'third contamination notification' do
      let(:survivor) { FactoryBot.create(:survivor) }
      let(:notifier) { FactoryBot.create(:survivor) }
      let(:other_notifier) { FactoryBot.create(:survivor) }
      let(:another_notifier) { FactoryBot.create(:survivor) }

      it 'create a new contamination notification for survivor' do
        notifier.notify_infection!(survivor)
        other_notifier.notify_infection!(survivor)

        expect{
          another_notifier.notify_infection!(survivor)
        }.to change(Contamination, :count)

        survivor.reload

        expect(survivor.contaminations_count).to eql(3)
        expect(survivor.infected?).to be_truthy
      end
    end

    context 'no create contamination notification for infected survidor' do
      let(:survivor) { FactoryBot.create(:survivor) }
      let(:notifier) { FactoryBot.create(:survivor) }
      let(:other_notifier) { FactoryBot.create(:survivor) }
      let(:another_notifier) { FactoryBot.create(:survivor) }
      let(:futher_notifier) { FactoryBot.create(:survivor) }

      it 'no create a new contamination notification for survivor' do
        notifier.notify_infection!(survivor)
        other_notifier.notify_infection!(survivor)
        another_notifier.notify_infection!(survivor)

        expect{
          futher_notifier.notify_infection!(survivor)
        }.to_not change(Contamination, :count)

        survivor.reload

        expect(survivor.contaminations_count).to eql(3)
        expect(survivor.infected?).to be_truthy
      end
    end
  end
end
