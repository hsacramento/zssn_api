# == Schema Information
#
# Table name: contaminations
#
#  id          :integer          not null, primary key
#  survivor_id :integer
#  notifier_id :integer          not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'spec_helper'

RSpec.describe Contamination, type: :model do
  describe 'relationships' do
    it { should belong_to(:survivor) }
    it { should belong_to(:notifier) }
  end
end
